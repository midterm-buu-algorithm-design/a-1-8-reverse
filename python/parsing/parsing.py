##
# Anurak Yutthanawa
# 63160015 B.Sc. Computer Science
# #

import timeit

def main():
    num = input()
    value = parse(len(num), num, 0)
    print(value)
    print(type(value))

def parse(length, num, index):
    if length-1 == index:
        return ord(num[length-index-1]) - 48
    
    return (ord(num[length-index-1]) - 48) + (10 * parse(length, num, index+1))

start = timeit.default_timer()
main()
stop = timeit.default_timer()
print('Time: ', stop - start) 