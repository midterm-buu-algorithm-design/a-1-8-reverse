# Pseudo-Code

A-1.8: 

Code by: *Anurak yutthanawa*

Student ID: *63160015*

**B.Sc. Computer Science**

```bash
Create List lst

for loop i = 0 to 4:
    do
        Append input to lst
    done

for loop i = 0 to length of list / 2:
    do
        index i , index -(i+1) of lst = index -(i+1), i of lst 
    done
print lst
```


Time Complexity = O(n)

Space Complexity = O(1)

Runtime: Exist in all .out file 

Hand Written Pseudocode

![image info](Reverse-Python-Pseudo.jpg)