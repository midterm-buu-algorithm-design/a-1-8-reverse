##
# Anurak Yutthanawa
# 63160015 B.Sc. Computer Science
# #

from typing import List
import timeit

def main():
    lst: List[int] = []

    for i in range(5):
        lst.append(input())

    for i in range(len(lst)//2):
        lst[i], lst[-(i+1)] = lst[-(i+1)], lst[i]
        
    print(lst)


start = timeit.default_timer()
main()
stop = timeit.default_timer()
print('Time: ', stop - start) 