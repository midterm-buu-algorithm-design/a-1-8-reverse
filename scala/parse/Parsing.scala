import scala.compiletime.ops.string
import java.util.Scanner

object Parsing {
  def main(args: Array[String]) : Unit = {

    val t1 = System.nanoTime

    var kb = new Scanner(System.in)
    var input = kb.next().toCharArray()
    var value = parse(input.length, input, 0)
    println(value)
    println(value.getClass())

    println("Time: " + (System.nanoTime - t1) / 1e9d)
  }

  def parse(length: Int, num: Array[Char], index: Int) : Int = {
    if (length-1 == index) {
        return num(length-index-1).toByte - 48
    }

    return (num(length-index-1).toByte - 48) + (10 * parse(length, num, index+1))
  }
}
