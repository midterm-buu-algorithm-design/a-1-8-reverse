# Pseudo-Code

A-1.9: 

Code by: *Anurak yutthanawa*

Student ID: *63160015*

**B.Sc. Computer Science**

```bash 
function parse()
    input : - length of string
            - string of number
            - current index
    do
        if length - 1 equal to current index
            do
                return ascii of substring of number which index is ( length string - current index - 1 )  - 48
            done
        
        return (ascii of substring of number which index is ( length string - current index - 1 ) - 48) + (10 * parse function with same length and string but change current index + 1)
    done

function main()
    do
        input string into variable num

        extract num string to array of char and set to input variable

        call parse() function with (length of input, input, and 0) are index arguments set into value variable
        
        print value

        print type of value for check
    done
```

Time Complexity = O(n)

Runtime: Exist in all .out file 

Hand Written Pseudocode

![Hand written pseudocode](Parsing-Scala-Pseudo.jpg)