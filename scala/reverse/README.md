# Pseudo-Code

A-1.8: 

Code by: *Anurak yutthanawa*

Student ID: *63160015*

**B.Sc. Computer Science**

```bash
Create List lst with called input 5 string

for loop i = 0 to length of list / 2:
    do
        define tmp variable and set to equal lst index i
        let lst index i = lst index (lst.length-1) - i
        let lst index (lst.length-1) - i = tmp
    done

for loop i in lst
    do
        print i
    done
```


Time Complexity = O(n)

Runtime: Exist in all .out file 

Hand Written Pseudocode

![Hand written pseudocode](Reverse-Scala-Pseudo.jpg)