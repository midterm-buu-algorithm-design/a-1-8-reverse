import java.util.Scanner;

object Reverse {
    def main(args: Array[String]) : Unit = {
        val t1 = System.nanoTime

        var kb = new Scanner(System.in)

        var lst = Array(kb.next(), kb.next(), kb.next(), kb.next(), kb.next())

        for (i <- 0 to (lst.length - 1) / 2){
            var tmp = lst(i)
            lst(i) = lst((lst.length-1) - i)
            lst((lst.length-1) - i) = tmp
        }
        
        for (i <- lst) {
            print(i+" ")
        }
        println()

        println("Time: " +(System.nanoTime - t1) / 1e9d)
    }
}